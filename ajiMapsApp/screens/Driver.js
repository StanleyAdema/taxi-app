import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, TouchableHighlight, Button} from 'react-native';
import MapView,{Polyline, Marker} from 'react-native-maps';
import Driver from "./screens/Driver";
import Passenger from "./screens/Passenger";

import _ from "lodash"
import PolyLine from "@mapbox/polyline"

export default class Driver extends Component {

  constructor(props){
    super(props);
    this.state = {
      isDriver: false,
      isPassenger: false,
      error: "",
      latitude: 0,
      longitude: 0,
      destination: "",
      predictions:[],
      pointCoords: [],
    };
    this.onChangeDestinationDebounced = _.debounce(
      this.onChangeDestination,
      1000
    )
  }

  componentWillMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState( {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        })
        // this.getRouteDirections();
      },
      error => this.setState({error: error.message}),
      {enableHighAccuracy: true, maximumAge: 2000, timeout: 20000}
    )

  }

  async getRouteDirections(placeId, destinationName){
    try{
      const response = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${this.state.latitude},${this.state.longitude}&destination=place_id:${placeId}&key=AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw`)
      const json = await response.json()
      console.log(json)
      const points = PolyLine.decode(json.routes[0].overview_polyline.points)
      const pointCoords = points.map(point =>
        {
          return{latitude:point[0],longitude:point[1]}
        }
      )
      this.setState({pointCoords, predictions:[], destination:destinationName})

      this.map.fitToCoordinates(pointCoords, {
        edgePadding:{top:30, bottom:30, left:30, right:30}
      })
    } catch(error){
      console.error(error)
    }
  }

  async onChangeDestination(destination){
    const apikey = "AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw"
    const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apikey}
                        &input=${destination}&location=${this.state.latitude}, ${this.state.longitude}
                        &radius=2000`
    try{
      const result = await fetch(apiUrl)
      const json = await result.json()
      console.log("iniJson")
      console.log(json)
      this.setState({predictions: json.predictions})
    } catch (e) {
      console.log(e)
    }
  }

  render() {
    if (this.state.isDriver) {
      return <Driver />;
    }

    if (this.state.isPassenger) {
      return <Passenger />;
    }

    let marker = null;
    if(this.state.pointCoords.length >1){
      marker = (
        <Marker coordinate={this.state.pointCoords[this.state.pointCoords.length-1]}/>
      )
    }
    const predictions = this.state.predictions.map(prediction => (
      <TouchableHighlight
        key={prediction.id}
        onPress={() => this.getRouteDirections(prediction.place_id, prediction.description)}
      >
        <View>
          <Text style={styles.suggestions} key={prediction.id}>{prediction.description}</Text>
        </View>
      </TouchableHighlight>
    ))

    return (
      <View style={styles.container}>
        <Button
          onPress={() => this.setState({ isPassenger: true})}
        />
        <Button
          onPress={() => this.setState({isDriver: true})}
          title="Driver"
        />
        <MapView
          ref={ map =>
            this.map = map

          }

          style={styles.map}
          region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}
          showsUserLocation={true}
        >
          <Polyline
            coordinates={this.state.pointCoords}
            strokeWidth={2.5}
            strokeColor={"red"}
          />
          {marker}

        </MapView>
        <TextInput
          placeholder={"Enter Your Current Location"}
          value={this.state.destination}
          style={styles.destination}
          onChangeText={destination => {
            this.setState({destination})
            this.onChangeDestinationDebounced(destination)
          }}
        >

        </TextInput>
        {predictions}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  suggestions: {
    backgroundColor: '#fff',
    padding: 5,
    fontSize: 18,
    borderWidth: 0.5,
    marginHorizontal: 5,
  },

  destination: {
    padding: 10,
    height: 40,
    borderRadius: 10,
    marginTop: 50,
    marginHorizontal: 10,
    backgroundColor: '#fff'
  },
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
